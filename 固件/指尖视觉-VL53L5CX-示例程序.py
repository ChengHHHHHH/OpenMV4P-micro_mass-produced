'''
VL53L5CX驱动基于ST官方驱动及OpenMV原作者的适配。
程欢欢通过对固件的修改，将其启用。在官方固件中并不能调用此功能。

端口连接：
    SCL - P7
    SDA - P8

程欢欢
20230613
'''

import sensor, image, time, tof, screen
from pyb import millis

tof.init()  #初始化VL53L5CX

screen=screen.screen()

sensor.reset()
sensor.set_pixformat(sensor.RGB565)
sensor.set_framesize(sensor.QVGA)
sensor.set_windowing(320,172)
sensor.set_vflip(True)
sensor.set_hmirror(True)
clock = time.clock()

x_starting = 30
x_step = 21
y_step = 21

img = sensor.snapshot()

tof_depth = tof.read_depth()
tof_depth_last = tof_depth
img_block = sensor.alloc_extra_fb(x_step,y_step,sensor.RGB565)
while(True):
    clock.tick()
    #获取VL53L5CX数据。当前仅有8*8 15Hz模式。
    #就是说，运行这条程序的时间间隔小于66.67ms，会强制等待。以保持15Hz。
    #可以通过其他并行运行方式，在获取它的数据的同时，不降低系统速度。

    tof_depth = tof.read_depth()

    img = sensor.snapshot()

    n=0 #通过循环，将数据绘制到屏幕上。
    for y in range(8):
        for x in range(8):
            img_block.draw_rectangle(0, 0, x_step, y_step, color=(255,0,0),fill=True)
            alpha = round( (255 - (tof_depth[0][n] )) )
            if alpha < 0: alpha = 0

            img.draw_image(img_block, x_starting+x*x_step, y*y_step, alpha=alpha)
            n+=1
    n=0
    for y in range(8):
        for x in range(8):
            img.draw_string(round(x_starting+x*x_step), round((x%2)*(y_step/2)+y*y_step),\
                                 str(round(tof_depth[0][n])), scale=1 , x_spacing=-2)
            n+=1

    #还可以通过下面这行程序，将64个距离数据的列表，通过图形，叠加到画面上。
    #但这个库效果不理想，而且可调参数太少，所以在上面我自己写了显示程序。
    #tof.draw_depth(img,tof_depth[0],30,-10,alpha = 50,hint = image.BICUBIC)

    screen.display(img)
    print(clock.fps())

