'''
另一个比较完善的例程，可能存在BUG？
我运行一会，会卡死
这条程序则没有问题
所以一并奉上，作为参考

程欢欢
20230614
'''

import sensor, image, time, tof

tof.init()

sensor.reset()
sensor.set_pixformat(sensor.RGB565)
sensor.set_framesize(sensor.QVGA)
clock = time.clock()

while(True):
    clock.tick()
    img = sensor.snapshot()
    tof_depth = tof.read_depth()
    tof.draw_depth(img,tof_depth[0],30,-10,alpha = 50,hint = image.BICUBIC)
    print(clock.fps())
