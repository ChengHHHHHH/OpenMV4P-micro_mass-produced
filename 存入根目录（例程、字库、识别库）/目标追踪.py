'''
OpenMV4P_micro
模板匹配

本程序采用模板匹配+实时更新。可以在目标发生简单变动后依然识别。但长时间运行，目标变动较快时不可靠。

按右键录入目标，此时会自动对焦后再录入。
按左键取消录入。
程序默认使用P8端口连接舵机，实现舵机旋转驱动摄像头模块，对目标追踪。

B站：@程欢欢的智能控制集
QQ群：245234784、532887292
淘宝店铺：shop111000005.taobao.com
20221116
'''
import sensor, image, time,screen,button
from os import mkdir
from pyb import Servo,millis
s2 = Servo(2) # P8
s2.pulse_width(1500)
sensor.reset()
sensor.set_framesize(sensor.QQVGA)
sensor.set_windowing(160,86)
sensor.set_pixformat(sensor.RGB565)
sensor.set_vflip(True)
sensor.set_hmirror(True)

clock = time.clock()

degrees=0

screen=screen.screen()

face_cascade = image.HaarCascade("frontalface", stages=25)

img_grayscale = sensor.alloc_extra_fb(160,86, sensor.GRAYSCALE)
image_data = sensor.alloc_extra_fb(50,50, sensor.GRAYSCALE)

image_matching = sensor.alloc_extra_fb(50,50, sensor.GRAYSCALE)
face_size=[]

while True:
    #录入
    while True:
        s2.pulse_width(1500)
        img = sensor.snapshot()
        if button.right.state():
            sensor.ioctl(sensor.IOCTL_TRIGGER_AUTO_FOCUS)#自动对焦
            timer=millis()
            while millis()-timer<1000:
                img = sensor.snapshot()
                img.draw_rectangle(55,18,50,50,color=(255,255,0),thickness=2)
                screen.display(img,x_size=320)
            img = sensor.snapshot()
            image_data.draw_image(img,0,0,roi=(55,18,50,50))
            break
        img.draw_rectangle(55,18,50,50,thickness=2)
        screen.display(img,x_size=320)
    #识别
    while True:
        img = sensor.snapshot()
        img_grayscale.draw_image(img,0,0)
        img_grayscale.to_grayscale()
        results = img_grayscale.find_template(image_data, 0.70, step=2, search=image.SEARCH_EX)#匹配率70%追踪
        if results:
            image_matching.draw_image(img_grayscale,0,0,roi=results)
            if image_matching.find_template(image_data, 0.85, step=2, search=image.SEARCH_EX):#匹配率85%更新目标
                image_data.draw_image(image_matching,0,0)
            img.draw_rectangle(results,thickness=2,color=(255,0,0))
            print(results[0]+25-80)
            degrees -= ((results[0]+25-80)*1)
            if degrees>1000:    degress=1000
            elif degrees<-1000: degress=-1000
            s2.pulse_width(1500+degrees)
        if button.left.state():
            break
        screen.display(img,x_size=320)
