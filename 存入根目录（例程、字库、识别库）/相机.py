'''
OpenMV4P_micro
拍照程序

说明：
屏幕左上角显示的是当前照片数，当前模式像素。
照片数最大999，超过将会从0开始覆盖。
模式共4个，分别是：
    34w像素，可达最高45帧
    88W像素，可达28帧
    503W像素，10帧以下，感光器的最大值
    5.5W像素放大效果，利用感光器最大值，可达到8倍数码变焦
按左键切换拍摄像素
短按右键拍摄照片
长按右键（0.5秒以上）进行自动对焦。注意自动对焦最小距离约5cm，小于5cm可能对焦失败（保持模糊画面）。

B站：@程欢欢的智能控制集
QQ群：245234784、532887292
淘宝店铺：shop111000005.taobao.com
20221116
'''
import sensor, image, time,screen,button,os
from os import listdir,mkdir
from pyb import millis
sensor.reset()
sensor.set_pixformat(sensor.RGB565)
sensor.set_framesize(sensor.SVGA)
sensor.set_vflip(True)
sensor.set_hmirror(True)
sensor.set_windowing(800,430)

clock = time.clock()
screen=screen.screen()

try:mkdir('/photos') #如果没有photos文件夹，则创建
except:pass
image_files=listdir('/photos')   #扫描照片文件名
image_files.sort()    #文件排序

number=0#文件名序号

for n in range(len(image_files)-1,0,-1):#倒叙检索文件名，找到最后一个序号
    if image_files[n][:6]=='photo_' and image_files[n][9:]=='.bmp':#符合命名规则
        try:
            number=eval(image_files[n][6:9]) #获取最后一个照片的序号
            number+=1
            if number>999:
                number=0    #如果序号最大，则置0，循环覆盖
            break
        except:
            pass

def file_number_to_str(n):
    if n<10:
        return '00'+str(n)
    elif n<100:
        return '0'+str(n)
    else:
        return str(n)

img_display=sensor.alloc_extra_fb(320,172,sensor.RGB565)#显示所用画面
mode=0
mode_name='34W'
fps=0
img_display_x=0
#sensor初始化会自动设置这项三重缓冲功能，但因系统BUG，在过多占用内存时，三重缓冲功能会被取消（摄像头获取画面速度降低）。需要重新设置。
sensor.set_framebuffers(sensor.TRIPLE_BUFFER)
while(True):
    clock.tick()
    img = sensor.snapshot()
    if img_display_x>0:#画面不占全屏，主动擦除以免画面残留其他内容
        img_display.clear()
    img_display.draw_image(img,img_display_x,0,y_size=172)
    if button.right.state():
        timer = millis() #判断长按，阈值1s
        while (millis() - timer <= 500) and button.right.state():
            img = sensor.snapshot()
            img_display.draw_image(img,img_display_x,0,y_size=172)
            img_display.draw_string(19,11,file_number_to_str(number)+' | '+mode_name,scale=2,color=(0,0,0),x_spacing=1,mono_space=False)
            img_display.draw_string(18,10,file_number_to_str(number)+' | '+mode_name,scale=2,x_spacing=1,mono_space=False)
            img_display.draw_rectangle(286,35,20,8,color=(255,255,0))
            img_display.draw_rectangle(286,35,round((millis() - timer)/25),8,color=(255,255,0),fill=True)
            screen.display(img_display)
        if millis() - timer > 500:#长按
            sensor.ioctl(sensor.IOCTL_TRIGGER_AUTO_FOCUS)#自动对焦
            while button.right.state():
                img = sensor.snapshot()
                img_display.draw_image(img,img_display_x,0,y_size=172)
                img_display.draw_string(19,11,file_number_to_str(number)+' | '+mode_name,scale=2,color=(0,0,0),x_spacing=1,mono_space=False)
                img_display.draw_string(18,10,file_number_to_str(number)+' | '+mode_name,scale=2,x_spacing=1,mono_space=False)
                screen.display(img_display)
        else:  #短按
            while button.right.state():
                pass
            number+=1
            img_display.draw_image(img,img_display_x,0,y_size=172)
            stream = image.ImageIO('/photos/photo_'+file_number_to_str(number)+'.bin', "w")
            stream.write(img_display)
            stream.close()
            img_display.draw_rectangle(0,0,320,172,color=(0,0,0),fill=True)
            screen.display(img_display)
            img.save('/photos/photo_'+file_number_to_str(number)+'.bmp')
    if button.left.state():
        mode+=1
        if mode>=4:
            mode=0
        if mode==0:
            sensor.set_framesize(sensor.SVGA)
            sensor.set_windowing(800,430)
            mode_name='34W'
        if mode==1:
            sensor.set_framesize(sensor.HD)
            sensor.set_windowing(1280,688)
            mode_name='88W'
        if mode==2:
            sensor.set_framesize(sensor.WQXGA2)
            sensor.set_windowing(2592,1944)
            mode_name='503W'
            img_display_x=45
            img_display.clear()
        if mode==3:
            sensor.set_framesize(sensor.WQXGA2)
            sensor.set_windowing(320,172)
            mode_name='zoom up 8x 5.5W'
            img_display_x=0
        while button.left.state():
            pass
    img_display.draw_string(19,11,file_number_to_str(number)+' | '+mode_name,scale=2,color=(0,0,0),x_spacing=1,mono_space=False)
    img_display.draw_string(18,10,file_number_to_str(number)+' | '+mode_name,scale=2,x_spacing=1,mono_space=False)
    screen.display(img_display)
    fps=clock.fps()
    print(clock.fps())
