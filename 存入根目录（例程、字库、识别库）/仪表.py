'''
OpenMV4P_micro
仪表展示程序

通过曲线图显示XY轴光流移动幅度
展现设备的可能性~

按左键或右键，重置X或Y轴曲线

B站：@程欢欢的智能控制集
QQ群：245234784、532887292
淘宝店铺：shop111000005.taobao.com
20221116
'''
import sensor, image, time,screen,button
from text import font
sensor.reset()
sensor.set_pixformat(sensor.RGB565)
sensor.set_framesize(sensor.B64X64)
sensor.set_windowing(64,64)
extra_fb = sensor.alloc_extra_fb(64,64, sensor.RGB565)#光流所需的画布

screen=screen.screen()

img_now=sensor.alloc_extra_fb(320,172,sensor.RGB565)
img_last=sensor.alloc_extra_fb(320,172,sensor.RGB565)

y_now=172/2
y_last=172/2
x_now=172/2
x_last=172/2
cursor_y=0
sensor.set_framebuffers(sensor.TRIPLE_BUFFER)#重新设置摄像头三重缓冲，避免降速BUG
while True:
    img = sensor.snapshot()
    displacement = extra_fb.find_displacement(img)
    extra_fb.replace(img)

    if displacement.response() > 0.1:
        y_now += displacement.y_translation()
        x_now += displacement.x_translation()

    if y_now>170:
        y_now=170
    if y_now<2:
        y_now=2
    if x_now>170:
        x_now=170
    if x_now<2:
        x_now=2

    img_now.clear()
    img_now.draw_image(img_last,-5,0)
    img_now.draw_line(315,round(x_last),320,round(x_now),color=(0,0,255))
    img_now.draw_line(315,round(y_last),320,round(y_now),color=(255,0,0))
    img_last.draw_image(img_now,0,0)
    img_now.draw_text(font,5,5,'X轴光流：'+str(86-x_now),color=(0,0,255))
    img_now.draw_text(font,5,25,'y轴光流：'+str(86-y_now),color=(255,0,0))

    y_last=y_now
    x_last=x_now

    if button.left.state():
        x_now=x_last=86
    if button.right.state():
        y_now=y_last=86

    screen.display(img_now)
