'''
OpenMV4P_micro
二维码识别

因为扩大画面，所以帧速略低，但可以识别大画幅二维码。识别完成后显示字符，支持中英文字符（默认UTF8编码）。

按右键自动对焦
识别到二维码后：
    如果二维码内容没有超出屏幕显示范围，按右键进入条幅滚动模式，此时按右键调节滚动速度
    如果二维码内容超出屏幕显示范围，按右键翻页
    按左键退出显示界面，重新等待扫描

B站：@程欢欢的智能控制集
QQ群：245234784、532887292
淘宝店铺：shop111000005.taobao.com
20221218
'''
import sensor, image, time,screen,button
from text import font
from pyb import millis
sensor.reset()
sensor.set_framesize(sensor.HD)
sensor.set_windowing(1280,688)
sensor.set_pixformat(sensor.GRAYSCALE)
sensor.set_contrast(3)
sensor.set_vflip(True)
sensor.set_hmirror(True)

clock = time.clock()

screen=screen.screen()
img_display = sensor.alloc_extra_fb(320, 172, sensor.RGB565)
timer=millis()
while True:
    img = sensor.snapshot()
    codes = img.find_qrcodes()
    print(codes)

    if button.right.state() and millis()-timer>1000:
        timer=millis()
        sensor.ioctl(sensor.IOCTL_TRIGGER_AUTO_FOCUS)#自动对焦

    if codes:
        img_display.clear()
        length=(img_display.draw_text(font,5,20,codes[0][4],line_length=305))[1]-20+18 #字符16行间距2
        text_y=20
        print('长度：'+str(length))
        while not button.left.state():#按下左键退出显示，继续扫描
            if button.right.state() :
                if length>152:#长度大于屏幕高度172-20=152
                    if text_y+length>=(162):
                        for n in range(0,40,4):
                            img_display.clear()
                            img_display.draw_text(font,5,text_y-n,codes[0][4],line_length=305)
                            screen.display(img_display)
                        text_y-=40
                    else:
                        for n in range(0,(text_y*-1)+20,4):
                            img_display.clear()
                            img_display.draw_text(font,5,text_y+n,codes[0][4],line_length=305)
                            screen.display(img_display)
                        text_y=20
                    while button.right.state():
                        pass
                else:#长度不大于屏幕
                    while button.right.state():
                        pass
                    img_display.clear()
                    length_signle_line = img_display.draw_text(font,0,35,codes[0][4],scale = 8)[0]
                    length_signle_line += 320   #文字总长度+屏幕长度
                    print(length_signle_line)
                    roll_x = 320
                    roll_speed = 1
                    while not button.left.state():
                        roll_x -= roll_speed*2
                        if roll_x < -length_signle_line:
                            roll_x = 320
                        img_display.clear()
                        img_display.draw_text(font,roll_x,35,codes[0][4],scale = 8)
                        img_display.draw_text(font,20,10,'滚动速度：' + str(roll_speed))
                        screen.display(img_display)
                        if button.right.state():
                            roll_speed +=1
                            if roll_speed>10:
                                roll_speed = 1
                            while button.right.state():
                                pass

            screen.display(img_display)

    screen.display(img,x_size=320)
